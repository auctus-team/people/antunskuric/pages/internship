# Summary

* [Context](README.md#portable-solutions-for-human-arm-pose-tracking)
* [Mission](README.md#mission)
* [Requirements](README.md#requirements)
* [How to apply](README.md#applications)
* [Supervisors](README.md#supervision)
* [Location](README.md#location)

